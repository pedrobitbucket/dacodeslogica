//
//  main.swift
//  DaCodesLogica
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import Foundation

let nunarg = Int(CommandLine.argc - 1)
let input = CommandLine.arguments[1...nunarg]
let test = Int(CommandLine.arguments[1])
let dimension = Array(CommandLine.arguments[2...nunarg])
print("input: \(input.joined(separator: " "))")

var results = [String]()
for i in 0..<test! {
    let n = Int(dimension[i*2])
    let m = Int(dimension[(i*2)+1])
    let algorithm = Algorithm(n: n!, m: m!)
    let result = algorithm.start()
    results.append(result)
}
print("output: \(results.joined(separator: " "))")
