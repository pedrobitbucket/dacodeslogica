//
//  Algorithm.swift
//  DaCodesLogica
//
//  Created by Pedro Romero on 7/30/18.
//  Copyright © 2018 Predrusky. All rights reserved.
//

import Foundation

enum Direction {
    case right
    case down
    case left
    case up
}

class Algorithm {
    var grid = [[Int]]()
    var n = 0
    var m = 0
    var result = ""
    
    init(n: Int, m: Int) {
        self.n = n
        self.m = m
        self.grid = [[Int]](repeating: [Int](repeating: 0, count: m), count: n)
    }
    
    func start() -> String {
        move(row: 0, col: 0, direction: .right)
        return result
    }
    
    private func move(row: Int, col: Int, direction: Direction) {
        if !isFinish() {
            switch direction {
            case .right:
                if col < self.m && self.grid[row][col] == 0 {
                    self.grid[row][col] = 1
                    self.result = "R"
                    move(row: row, col: col + 1, direction: .right)
                } else {
                    move(row: row + 1, col: col - 1, direction: .down)
                }
            case .down:
                if row < self.n && self.grid[row][col] == 0 {
                    self.grid[row][col] = 1
                    self.result = "D"
                    move(row: row + 1, col: col, direction: .down)
                } else {
                    move(row: row - 1, col: col - 1, direction: .left)
                }
            case .left:
                if col >= 0 && self.grid[row][col] == 0 {
                    self.grid[row][col] = 1
                    self.result = "L"
                    move(row: row, col: col - 1, direction: .left)
                } else {
                    move(row: row - 1, col: col + 1, direction: .up)
                }
            case .up:
                if row >= 0 && self.grid[row][col] == 0 {
                    self.grid[row][col] = 1
                    self.result = "U"
                    move(row: row - 1, col: col, direction: .up)
                } else {
                    move(row: row + 1, col: col + 1, direction: .right)
                }
            }
        }
    }
    
    private func isFinish() -> Bool {
        for x in 0..<n {
            for y in 0..<m {
                if grid[x][y] == 0 {
                    return false
                }
            }
        }
        return true
    }
}
