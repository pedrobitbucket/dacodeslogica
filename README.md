# Prueba Logica
Este repositorio contiene el proyecto correspondiente a la [prueba de Logica](https://bitbucket.org/dacodes/pruebas/src/master/Logic/) de DaCodes.

## Instrucciones
Para poder correr el proyecto se deben seguir los siguientes pasos:

  1. Clonar el proyecto en su equipo.

    ```
    git clone https://bitbucket.org/pedrobitbucket/dacodeslogica.git
    ```

  2. Abrir el proyecto con el Xcode.
  3. Editar los esquemas para poder pasar los argumentos al programa.

    ![editSquema](/screenshots/editScheme.png)

  4. En la pestaña de *Argumentos*, presionar el boton con el signo de mas *+* y agregar los parametros. Por ejemplo: 3 1 1 2 2 3 3.

    ![addParameters](/screenshots/addParameters.png)
    
  5. Ejecutar la aplicacion en la parte de la consola se debe mostrar el resultado.

Para cualquier duda mandar un corre a pedro128@gmail.com
